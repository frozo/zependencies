use std::{
    collections::BTreeSet,
    fmt,
    fs::File,
    io::{stdout, BufWriter, Write},
    path::PathBuf,
    time::Instant,
};

use chrono::{DateTime, Utc};
use clap::{Parser, ValueEnum};
use color_eyre::{eyre::bail, Result};
use db_dump::{crates::CrateId, dependencies::DependencyKind, versions::VersionId, DbDump};
use fxhash::FxHashSet;
use petgraph::{dot, prelude::*, stable_graph::DefaultIx};
use rayon::prelude::{IntoParallelRefIterator, ParallelIterator};
use semver::{Version, VersionReq};

type FxHashMap<K, V> = hashbrown::HashMap<K, V, fxhash::FxBuildHasher>;

#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: tikv_jemallocator::Jemalloc = tikv_jemallocator::Jemalloc;

#[derive(Parser)]
#[command(author, version, about)]
struct Args {
    /// The crate to be queried.
    name: String,
    /// A cargo compatible semver version requirement.
    #[arg(short = 'v', long, default_value_t = VersionReq::STAR)]
    version: VersionReq,
    /// If set, ignores whether versions are outdated.
    #[arg(short = 'a', long)]
    all_versions: bool,
    /// Configure whether to collapse duplicate trees. Only applicable on tree-like outputs (JSON, KDL).
    #[arg(short = 'c', long)]
    no_collapse_duplicates: bool,
    /// The kind of dependencies that should be included.
    #[arg(short = 'k', long, value_enum, default_values_t = [DepKind::Normal, DepKind::Build, DepKind::Dev])]
    kind: Vec<DepKind>,
    /// If set, will filter out optional dependencies.
    #[arg(short = 'r', long)]
    required_only: bool,
    /// Whether to include yanked dependencies.
    #[arg(short = 'y', long)]
    include_yanked: bool,
    /// The number of downloads a version needs to have to be included.
    #[arg(short = 'd', long, default_value_t = 0)]
    min_downloads: u64,
    /// The UTC-relative date and time the crate has to have been last updated.
    #[arg(short = 'u', long, default_value_t = Default::default())]
    min_updated: DateTime<Utc>,
    /// How the results should be outputted.
    #[arg(short = 'f', long, value_enum)]
    output_format: Format,
    /// Into which file the output should be saved. If not set, standard output will be used.
    #[arg(short = 'o', long)]
    output_file: Option<PathBuf>,
    /// The path of the crates.io database dump.
    #[arg(long, default_value = "db-dump.tar.gz")]
    db_dump_path: PathBuf,
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum DepKind {
    Normal,
    Build,
    Dev,
}

impl From<DepKind> for DependencyKind {
    fn from(value: DepKind) -> Self {
        match value {
            DepKind::Normal => DependencyKind::Normal,
            DepKind::Build => DependencyKind::Build,
            DepKind::Dev => DependencyKind::Dev,
        }
    }
}

impl From<DependencyKind> for DepKind {
    fn from(value: DependencyKind) -> Self {
        match value {
            DependencyKind::Normal => DepKind::Normal,
            DependencyKind::Build => DepKind::Build,
            DependencyKind::Dev => DepKind::Dev,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, ValueEnum)]
enum Format {
    /// It's JSON.
    Json,
    /// A human readable and writable format: KDL.
    Kdl,
    /// Graphviz compatible output.
    Dot,
    /// Prints the number of dependents to the given crate.
    Count,
}

fn main() -> Result<()> {
    color_eyre::install()?;

    let args = Args::parse();

    let mut db = DbDump::default();
    db_dump::Loader::new()
        .crates(|row| db.crates.push(row))
        .dependencies(|row| db.dependencies.push(row))
        .versions(|row| db.versions.push(row))
        .load(args.db_dump_path)?;
    let index = db.index();

    let leaf_version_ids = resolve_versions(&db, &args.name, &args.version);
    if leaf_version_ids.is_empty() {
        bail!("Could not resolve crate with the specified version.");
    };

    let time = Instant::now();
    let latest = determine_latest_versions(&db, &index);

    let full_graph = build_dependency_tree(
        &db,
        |dependent| {
            args.kind.contains(&dependent.kind.into())
                && (!args.required_only || !dependent.optional)
        },
        |version| (args.include_yanked || !version.yanked),
    );
    let filter = |version_id| {
        (args.all_versions || latest.contains(&version_id)) && {
            let version = index.version(version_id);
            version.downloads >= args.min_downloads && version.updated_at >= args.min_updated
        }
    };
    let graph = find_dependents(&full_graph, &leaf_version_ids, filter);

    eprintln!(
        "Finished searching for dependents in {}ms. Collected {} crate versions.",
        time.elapsed().as_millis(),
        graph.node_count()
    );

    let graph = graph.into_graph::<DefaultIx>();
    let leaf_node_indices: Vec<_> = graph
        .node_indices()
        .filter(|&node_idx| leaf_version_ids.contains(&graph[node_idx]))
        .collect();
    let named_graph = graph.map(
        |_, &version_id| {
            let version = index.version(version_id);
            DisplayCrate {
                name: &index.krate(version.crate_id).name,
                version: &version.num,
                outdated: !latest.contains(&version_id),
            }
        },
        |_, ()| (),
    );
    match args.output_format {
        Format::Json => {
            if let Some(output_file) = args.output_file {
                write_json(
                    BufWriter::new(File::create(output_file)?),
                    &leaf_node_indices,
                    named_graph,
                    !args.no_collapse_duplicates,
                )?;
            } else {
                write_json(
                    BufWriter::new(stdout().lock()),
                    &leaf_node_indices,
                    named_graph,
                    !args.no_collapse_duplicates,
                )?;
            }
        }
        Format::Kdl => {
            if let Some(output_file) = args.output_file {
                write_kdl(
                    BufWriter::new(File::create(output_file)?),
                    &leaf_node_indices,
                    named_graph,
                    !args.no_collapse_duplicates,
                )?;
            } else {
                write_kdl(
                    BufWriter::new(stdout().lock()),
                    &leaf_node_indices,
                    named_graph,
                    !args.no_collapse_duplicates,
                )?;
            }
        }
        Format::Dot => {
            let dot = dot::Dot::with_config(&named_graph, &[dot::Config::EdgeNoLabel]);
            if let Some(output_file) = args.output_file {
                writeln!(File::create(output_file)?, "{:?}", dot)?;
            } else {
                println!("{:?}", dot);
            }
        }
        Format::Count => {
            println!("{}", graph.node_count() - 1);
        }
    }

    Ok(())
}

fn resolve_versions(db: &DbDump, crate_name: &str, crate_version: &VersionReq) -> Vec<VersionId> {
    let Some(crate_id) = db
        .crates
        .iter()
        .find(|row| row.name == crate_name)
        .map(|row| row.id) else { return Vec::new(); };
    db.versions
        .iter()
        .filter(|row| row.crate_id == crate_id && crate_version.matches(&row.num))
        .map(|version| version.id)
        .collect()
}

fn determine_latest_versions(db: &DbDump, index: &db_dump::Index) -> FxHashSet<VersionId> {
    let mut latest = FxHashMap::with_hasher(Default::default());
    for version in &db.versions {
        let entry = latest.entry(version.crate_id).or_insert(version.id);
        if index.version(*entry).num < version.num {
            *entry = version.id;
        }
    }
    latest.into_values().collect()
}

fn build_dependency_tree<Df, Vf>(
    db: &DbDump,
    dependency_filter: Df,
    version_filter: Vf,
) -> GraphMap<VersionId, (), Directed>
where
    Df: Send + Sync + Fn(&db_dump::dependencies::Row) -> bool,
    Vf: Send + Sync + Fn(&db_dump::versions::Row) -> bool,
{
    let mut versions_by_crate: FxHashMap<CrateId, Vec<&db_dump::versions::Row>> =
        FxHashMap::with_capacity_and_hasher(db.versions.len(), Default::default());
    for row in &db.versions {
        versions_by_crate.entry(row.crate_id).or_default().push(row);
    }

    let edges: Vec<(VersionId, VersionId)> = db
        .dependencies
        .par_iter()
        .filter_map(|dependent| {
            if !dependency_filter(dependent) {
                return None;
            }
            let best_match = versions_by_crate
                .get(&dependent.crate_id)
                .into_iter()
                .flatten()
                .filter(|version| version_filter(version) && dependent.req.matches(&version.num))
                .max_by_key(|version| &version.num)?
                .id;
            Some((dependent.version_id, best_match))
        })
        .collect();
    edges.into_iter().collect()
}

fn find_dependents<F>(
    full_graph: &GraphMap<VersionId, (), Directed>,
    leaf_ids: &[VersionId],
    filter: F,
) -> GraphMap<VersionId, (), Directed>
where
    F: Sync + Send + Fn(VersionId) -> bool,
{
    fn depth_first_seq<F>(
        full_graph: &GraphMap<VersionId, (), Directed>,
        leaf_ids: &[VersionId],
        filter: &F,
    ) -> GraphMap<VersionId, (), Directed>
    where
        F: Fn(VersionId) -> bool,
    {
        let mut graph = GraphMap::new();
        let mut visited = BTreeSet::new();
        let mut path = Vec::new();
        for &leaf_id in leaf_ids {
            path.push(leaf_id);
            while let Some(&dependency_id) = path.last() {
                match full_graph
                    .neighbors_directed(dependency_id, Direction::Incoming)
                    .find(|&dependent_id| visited.insert((dependent_id, dependency_id)))
                {
                    Some(dependent_id) => {
                        path.push(dependent_id);
                    }
                    None => {
                        let dependent_id = path.pop().unwrap();
                        let Some(&dependency_id) = path.last() else { continue; };
                        if graph.contains_node(dependent_id) || filter(dependent_id) {
                            graph.add_edge(dependent_id, dependency_id, ());
                        }
                    }
                };
            }
        }
        graph
    }

    depth_first_seq(full_graph, leaf_ids, &filter)
}

struct DisplayCrate<'a> {
    name: &'a str,
    version: &'a Version,
    outdated: bool,
}

impl<'a> fmt::Display for DisplayCrate<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}@{}", self.name, self.version)?;
        if self.outdated {
            write!(f, " (o)")?;
        }
        Ok(())
    }
}

impl<'a> fmt::Debug for DisplayCrate<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(self, f)
    }
}

fn write_json<W: Write>(
    mut writer: W,
    leaf_node_indices: &[NodeIndex<DefaultIx>],
    named_graph: Graph<DisplayCrate, (), Directed>,
    collapse_duplicates: bool,
) -> Result<()> {
    let mut path_nodes = Vec::new();
    let mut layers = Vec::new();
    let mut visited = FxHashSet::with_capacity_and_hasher(0, Default::default());

    write!(&mut writer, "{{")?;
    let mut wants_comma = false;
    for &node_idx in leaf_node_indices {
        let node = named_graph.node_weight(node_idx).unwrap();
        if wants_comma {
            write!(&mut writer, ",")?;
        }
        let collapsed = collapse_duplicates && !visited.insert(node_idx);
        write!(
            &mut writer,
            "\"{name}@{version}\":{{\"outdated\":{outdated},\"collapsed\":{collapsed}",
            name = node.name,
            version = node.version,
            outdated = node.outdated,
        )?;
        wants_comma = true;

        if collapsed {
            write!(&mut writer, "}}")?;
            continue;
        }
        path_nodes.push(node_idx);
        layers.push(named_graph.neighbors_directed(node_idx, Direction::Incoming));

        while let Some(layer) = layers.last_mut() {
            match layer.next() {
                Some(node_idx) => {
                    if path_nodes.contains(&node_idx) {
                        // In case there is a cycle for some reason.
                        continue;
                    }

                    let node = named_graph.node_weight(node_idx).unwrap();
                    let collapsed = collapse_duplicates && !visited.insert(node_idx);
                    write!(
                        &mut writer,
                        ",\"{name}@{version}\":{{\"outdated\":{outdated},\"collapsed\":{collapsed}",
                        name = node.name,
                        version = node.version,
                        outdated = node.outdated,
                    )?;
                    if collapsed {
                        write!(&mut writer, "}}")?;
                        continue;
                    }
                    path_nodes.push(node_idx);
                    layers.push(named_graph.neighbors_directed(node_idx, Direction::Incoming));
                }
                None => {
                    path_nodes.pop();
                    layers.pop();
                    write!(&mut writer, "}}")?;
                }
            }
        }
    }
    write!(&mut writer, "}}")?;
    Ok(())
}

fn write_kdl<W: Write>(
    mut writer: W,
    leaf_node_indices: &[NodeIndex<DefaultIx>],
    named_graph: Graph<DisplayCrate, (), Directed>,
    collapse_duplicates: bool,
) -> Result<()> {
    let mut path_nodes = Vec::new();
    let mut layers = Vec::new();
    let mut visited = FxHashSet::with_capacity_and_hasher(0, Default::default());

    for &node_idx in leaf_node_indices {
        let node = named_graph.node_weight(node_idx).unwrap();
        write!(
            &mut writer,
            "dep \"{name}\" version=\"{version}\"",
            name = node.name,
            version = node.version,
        )?;
        if node.outdated {
            write!(&mut writer, " outdated=true")?;
        }

        let mut dependent_iter = named_graph
            .neighbors_directed(node_idx, Direction::Incoming)
            .peekable();
        if collapse_duplicates && !visited.insert(node_idx) {
            write!(&mut writer, " collapsed=true")?;
        } else if dependent_iter.peek().is_some() {
            writeln!(&mut writer, " {{")?;
            path_nodes.push(node_idx);
            layers.push(dependent_iter);
        } else {
            writeln!(&mut writer)?;
            continue;
        }

        while let Some(layer) = layers.last_mut() {
            match layer.next() {
                Some(node_idx) => {
                    if path_nodes.contains(&node_idx) {
                        // In case there is a cycle for some reason.
                        continue;
                    }

                    let mut dependent_iter = named_graph
                        .neighbors_directed(node_idx, Direction::Incoming)
                        .peekable();

                    let node = named_graph.node_weight(node_idx).unwrap();
                    for _ in 0..layers.len() {
                        write!(&mut writer, "  ")?;
                    }
                    write!(
                        &mut writer,
                        "dep \"{name}\" version=\"{version}\"",
                        name = node.name,
                        version = node.version,
                    )?;
                    if node.outdated {
                        write!(&mut writer, " outdated=true")?;
                    }
                    if collapse_duplicates && !visited.insert(node_idx) {
                        write!(&mut writer, " collapsed=true")?;
                    } else if dependent_iter.peek().is_some() {
                        write!(&mut writer, " {{")?;
                        path_nodes.push(node_idx);
                        layers.push(dependent_iter);
                    }
                    writeln!(&mut writer)?;
                }
                None => {
                    path_nodes.pop();
                    layers.pop();

                    for _ in 0..layers.len() {
                        write!(&mut writer, "  ")?;
                    }
                    writeln!(&mut writer, "}}")?;
                }
            }
        }
    }
    Ok(())
}
