# Zependencies
A tool to search for crates on crates.io that depend on a crate.

It tries to be a useful tool in the removal of unsound dependencies in the
wider rust ecosystem. Thus it provides a number of filtering options and
multiple output formats.

## Examples
![A dependent tree for traitobject 0.1.0](https://i.imgur.com/ygcluH6.png)

To search for crates that depend on the latest version of `traitobject` with at
least 10000 downloads and excluding optional dependencies. It then outputs a file
that can be used by Graphviz to be converted into a viewable SVG.
```sh
$ zependencies --output-format dot --min-downloads 10000 --required-only traitobject \ 
    | dot -Tsvg > traitobject.svg
```

Crates from 2023 and onwards that depend on rustc-serialize as a normal
(neither development, nor build) dependency. The output is formatted in JSON.
```sh
$ zependencies --output-format json --kind normal --min-updated '2023-01-01 00:00:00 UTC' rustc-serialize
```

## Prerequisites
You need to download the crates.io database dump. Zependencies has to be run in
the same folder, or a `--db-dump-path` argument has to be given.
```sh
$ curl -O https://static.crates.io/db-dump.tar.gz
```

## Installation
```sh
$ git clone https://gitlab.com/frozo/zependencies.git
$ cd zependencies
$ cargo install --path .
```
